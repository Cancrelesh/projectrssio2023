# Project R : Dans ma rue

Projet universitaire de Data Analyse.
Realise via Shiny App en R, ce projet reprent les donnes des rapport 
d'incidences dans la ville de Paris sur la periode 2021-2022.
Lien vers l'app : https://dans-ma-rue.shinyapps.io/home/

Lien vers le site de la ville de Paris dont nous nous sommes inspires [ici](https://dashboard.paris/explore/dataset/dans-ma-rue-anomalies-signalees/information/?disjunctive.type&disjunctive.soustype&disjunctive.code_postal&disjunctive.arrondissement&disjunctive.conseilquartier&disjunctive.prefixe&disjunctive.intervenant)

Lien vers les donnees utilsees lors de ce projet [ici](https://dashboard.paris/explore/dataset/dans-ma-rue-anomalies-signalees/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=%3B)
